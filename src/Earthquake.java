import java.util.*;
public class Earthquake {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		double val = s.nextDouble();
		if(val<0) {
			System.out.println("Invalid input, please enter a value above 0");
		}
		else if(val<4.5&&val>=0) {
			System.out.println("No destruction of buildings");
		}
		else if(val<6.0&&val>=4.5) {
			System.out.println("Damage to poorly constructed Building");
		}
		else if(val<7.0&&val>=6.0) {
			System.out.println("Many buildings considerably damaged, some collapse");
		}
		else if(val<8.0&&val>=7.0) {
			System.out.println("Many buildings destroyed");
		}
		else {
			System.out.println("Most structures fall");
		}
	}

}
